
var Sound = cc.Class({
    name: 'Sound',
    properties: {
        url: cc.AudioClip
    },
});

cc.Class({
    extends: cc.Component,

    properties: {
        sounds: {
            default: [],
            type: Sound,
        },

        buttonSE: {
            default: null,
            url: cc.AudioClip
        },

        closeSE: {
            default: null,
            url: cc.AudioClip
        },

        darknessBGM: {
            default: null,
            url: cc.AudioClip
        },

        audio: cc.AudioSource,
    },

    // onLoad () {},

    start() {
    },

    playSE(No) {
        if (No == 2) {
            cc.audioEngine.play(this.sounds[No].url, false, 1.1);
            return;
        }
        cc.audioEngine.play(this.sounds[No].url, false);
    },

    pushButton() {
        cc.audioEngine.play(this.buttonSE);
    },

    pushClose() {
        cc.audioEngine.play(this.closeSE);
    },

    changeBGM() {
        this.audio.clip = this.darknessBGM;
    },

    playBGM() {
        this.audio.play();
    },

    stopBGM() {
        this.audio.stop();
    },

    // update (dt) {},
});
