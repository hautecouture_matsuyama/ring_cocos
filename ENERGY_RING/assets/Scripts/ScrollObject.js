
var pointL = -1300;
var pointR = 1300;
var scrollValue = 3360;

cc.Class({
    extends: cc.Component,

    properties: {
        speed: 100
    },

    // onLoad () {},

    start() {
        this.scroll();
    },

    scroll() {
        var forever = cc.repeatForever(cc.moveBy(0.2, this.speed, 0));
        this.node.runAction(forever);
    },

    update(dt) {
        var posX = this.node.getPositionX();
        if (posX > pointR && this.speed > 0) {
            this.node.setPositionX(posX - scrollValue);
        }
        else if (posX < pointL && this.speed < 0) {
            this.node.setPositionX(posX + scrollValue);
        }
            
    },
});
