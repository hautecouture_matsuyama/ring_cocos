

cc.Class({
    extends: cc.Component,

    properties: {
        player: cc.Node,
    },

    // onLoad () {},

    shake() {

        var shake1 = cc.moveBy(0.05, 10, 10);
        var shake2 = cc.moveBy(0.05, -10, -10);
        var shake3 = cc.moveBy(0.05, 7, 7);
        var shake4 = cc.moveBy(0.05, -7, -7);
        var shake5 = cc.moveBy(0.05, 5, 5);
        var shake6 = cc.moveBy(0.05, -5, -5);
        var shake7 = cc.moveBy(0.05, 2, 2);
        var shake8 = cc.moveBy(0.05, -2, -2);

        var seq1 = cc.sequence(shake1, shake2, shake1, shake2, shake3, shake4, shake3, shake4);
        var seq2 = cc.sequence(shake5, shake6, shake5, shake6, shake7, shake8, shake7, shake8);

        this.node.runAction(cc.sequence(seq1, seq1,seq2,seq2));

        

    },

    // start() {},

    update(dt) {

        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        if (game.getIsEnd() == true)
            return;
        
        if (this.player.getPositionY() > this.node.position.y) {
            this.node.position = this.player.getPosition();
        }
    },
});
