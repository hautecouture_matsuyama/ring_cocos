
var ScoreManager = require('ScoreManager');
var SoundManager = require('SoundManager');
cc.Class({
    extends: cc.Component,

    properties: {
        body: cc.RigidBody,

        colorTag: 2,     //RGBR 0123

        score: ScoreManager,
        sound: SoundManager,

        colors: {
            default: [],
            type: cc.Node
        },
    },

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getCollisionManager().enabled = true;
    },

    jump() {
        this.sound.playSE(2);
        this.body.linearVelocity = cc.p(0, 700);
    },

    changeColor(other) {
        if (other.tag == 1) {
            do {
                this.colorTag = Math.floor(Math.random() * 4);
            } while (this.colorTag == 0)
        }
        else if (other.tag == 2) {
            do {
                this.colorTag = Math.floor(Math.random() * 4);
            } while (this.colorTag == 2)
        }
        else
            this.colorTag = Math.floor(Math.random() * 4);
        for (var i = 0; i < 4; i++) {
            if (i == this.colorTag)
                this.colors[i].active = true;
            else
                this.colors[i].active = false;
        }

    },

    start() {

    },

    onCollisionEnter(other, self) {
        if (other.node.name == 'Start_Wall')
            return;

        else if (other.node.name == 'Star') {
            this.score.countScore();
            this.sound.playSE(3);
            other.node.destroy();
            return;
        }

        else if (other.node.name == 'SpecialStar') {
            this.score.threeCount();
            this.sound.playSE(3);
            other.node.destroy();
            return;
        }

        else if (other.node.name == '4Color') {
            this.changeColor(other);
            this.sound.playSE(0);
            var spawn = cc.find('Canvas/GameManager').getComponent('SpawnSystem');
            spawn.spawnPlatform();
            other.node.destroy();
            return;
        }

        var plat = other.node.getComponent('PlatformColor');
        if (plat.getTag() != this.colorTag) {
            this.score.setScore();
            this.sound.playSE(1);
            var game = cc.find('Canvas/GameManager').getComponent('GameManager');
            game.gameOver();
            var camera = cc.find('Canvas/Camera').getComponent('CameraControl');
            camera.shake();
        }
    },

    // update(dt) {},
});
