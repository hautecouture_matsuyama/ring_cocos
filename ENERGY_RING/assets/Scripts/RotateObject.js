

cc.Class({
    extends: cc.Component,

    properties: {
        angle: 360,         //負の数で左回り
        isSpecial: false,   //星や変化玉じゃなければfalse
    },

    // LIFE-CYCLE CALLBACKS:

    onEnable() {
        if (this.isSpecial) {
            this.sequence = this.Rotate();
            this.node.runAction(this.sequence);
        }
    },

    onDestroy() {
        // cc.log(this.node.name + 'が削除されました。');
    },

    Rotate() {
        var rotate = cc.rotateBy(2, 60);
        var _rotate = cc.rotateBy(2, -60);

        return cc.repeatForever(cc.sequence(rotate, _rotate));
    },

    // onLoad () {},

    // start () {},

    update(dt) {
        if (!this.isSpecial) {
            this.node.rotation += this.angle * dt;
        }

        
    },
});
