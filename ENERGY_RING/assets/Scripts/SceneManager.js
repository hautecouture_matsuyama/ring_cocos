
var Fade = require('Fade');

cc.Class({
    extends: cc.Component,

    properties: {
        fade: Fade,

        isChange: false,
    },

    change() {
        if (this.isChange)
            return;
        this.isChange = true;

        var sceneName = cc.director.getScene().name;

        // cc.log('現在のシーン名：' + sceneName);

        if(sceneName=='Home' || sceneName=='End') {
            this.fade.fadeIn();
            setTimeout(() => {
                cc.director.loadScene('Game');
            }, 1500);
        }
        else {
            cc.director.loadScene('End');
        }
    },

    goHome() {
        if (this.isChange)
            return;
        this.isChange = true;

        this.fade.fadeIn();
        setTimeout(() => {
            cc.director.loadScene('Home');
        }, 1500);
    },

    // onLoad () {},

    // start () {},

    // update (dt) {},
});
