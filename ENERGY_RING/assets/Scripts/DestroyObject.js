

cc.Class({
    extends: cc.Component,

    properties: {
        isDead: false,
        isTube: false,
    },

    // onLoad () {},

    start() {

    },

    setIsTube(value) {
        this.isTube = value;
    },

    getIsTube() {
        return this.isTube;
    },

    update(dt) {
        var camera = cc.find('Canvas/Camera');
        if (camera.getPositionY() - cc.visibleRect.height / 2 > this.node.getPositionY() + this.node.height && !this.isDead) {
            this.isDead = true;
            this.node.destroy();
        }
    },
});
