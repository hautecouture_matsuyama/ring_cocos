
var Player = require('Player');
var SoundManager = require('SoundManager');

cc.Class({
    extends: cc.Component,

    properties: {

        dieParticle: cc.Node,
        player: Player,
        sound: SoundManager,
        isGameOver: false,

        darkness: cc.Node,
    },

    onLoad() {
        this.setTouchEvent();
    },

    setTouchEvent() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouch: true,

            onTouchBegan: function (touch, event) {
                self.setJump();
                return false;
            }
        }, self.node);
    },

    setJump() {
        if(!this.isGameOver)
            this.player.jump();
    },

    getIsEnd() {
        return this.isGameOver;
    },

    gameOver() {
        if (this.isGameOver)
            return;
        
        this.isGameOver = true;
        this.dieParticle.active = true;

        this.sound.stopBGM();

        this.player.node.destroy();

        setTimeout(() => {
            var sceneManager = cc.find('Canvas/SceneManager').getComponent('SceneManager');
            sceneManager.change();
        }, 3000);
    },

    start() {
        var isClassic = Number(cc.sys.localStorage.getItem('MODE'));
        if (isClassic == 0) {
            this.darkness.active = true;
            this.sound.changeBGM();
        }
        this.sound.playBGM();
    },

    // update (dt) {},
});
