
//  IDの直入力でないとエラーが発生するので注意。

var preloadedInterstitial = null;

cc.Class({
    extends: cc.Component,

    properties: {
    },

    // onLoad () {},

    start() {
        this.initialize();
    },

    initialize() {
        var self = this;

        var canvas = cc.find('Canvas').getComponent('CanvasManager');

        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            canvas.preLoad();
            return;
        }

        FBInstant.getInterstitialAdAsync(
            '301556503869023_301557040535636',
        ).then(function (interstitial) {
            self.preloadedInterstitial = interstitial;
            return self.preloadedInterstitial.loadAsync();
        }).then(function () {
            console.log('Interstitial：Success!');
            canvas.setInterPossible();
            canvas.preLoad();
        }).catch(function (err) {
            console.log('Interstitial：Failed.');
            this.isPossible = false;

            //PC版で失敗した場合は広告ボタンは表示しない。
            if (navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/i)) {
            }else {
                canvas.remakeLayout();
            }
            canvas.preLoad();
        });

        
    },

    showAd(isReward) {
        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            console.log('Display failed.');
            return;
        }

        var self = this;

        if (self.preloadedInterstitial == null || typeof self.preloadedInterstitial == 'undefined')
            return;

        self.preloadedInterstitial.showAsync()
            .then(function () {
                console.log('Success!');
                if (isReward) {
                    var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
                    canvasManager.remakeLayout();
                }
            })
            .catch(function (e) {
                console.log(e.message);
            });
    }

    // update (dt) {},
});
