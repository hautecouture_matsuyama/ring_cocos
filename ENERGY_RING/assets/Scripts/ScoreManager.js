

cc.Class({
    extends: cc.Component,

    properties: {
        nowScore: 0,
        bestScore: 0,

        nowLabel: cc.Label,
        bestLabel: cc.Label,
    },

    // onLoad () {},

    start() {
        this.takeOverScore();
        //ゲームオーバー画面の時は最初に各スコアの表示を行う。
        if (cc.director.getScene().name == 'End') {
            this.nowLabel.string = 'SCORE\n' + this.nowScore;
            this.bestLabel.string = 'BEST\n' + this.bestScore;
            //ランキングデータに書き出し。
            var board = cc.find('Canvas/leader_board').getComponent('Leaderboard');
            board.sendScore(this.bestScore);
        }
    },

    countScore() {
        this.nowScore++;
        this.nowLabel.string = this.nowScore;
    },

    threeCount(){
        this.nowScore += 3;
        this.nowLabel.string = this.nowScore;
    },

    takeOverScore() {
        //スコア表示目的以外では値を引き継がない
        if (cc.director.getScene().name == 'End') {
            var now = cc.sys.localStorage.getItem('NOW');
            if (now != null)
                this.nowScore = Number(now);
        }

        //コンティニュー状態なら値を引き継ぐ
        if (cc.director.getScene().name == 'Game') {
            var continueNum = cc.sys.localStorage.getItem('Continue');
            if (continueNum != null && continueNum == 1) {
                var now = cc.sys.localStorage.getItem('NOW');
                if (now != null) {
                    this.nowScore = Number(now);
                    this.nowLabel.string = this.nowScore;
                    // console.log('スコア継承');
                }
            }
        }
        
        var best = cc.sys.localStorage.getItem('BEST');
        if (best != null)
            this.bestScore = Number(best);
    },

    setScore() {
        cc.sys.localStorage.setItem('NOW', this.nowScore);
        if (this.nowScore > this.bestScore) {
            cc.sys.localStorage.setItem('BEST', this.nowScore);
        }
    },

    // update (dt) {},
});
