
cc.Class({
    extends: cc.Component,

    properties: {
        platforms: {
            default: [],
            type: cc.Prefab
        },

        starPrefab: cc.Prefab,
        specialPrefab: cc.Prefab,
        colorPrefab: cc.Prefab,

        platformParent: cc.Node,
        starParent: cc.Node,
        colorParent: cc.Node,

        prevNode: cc.Node,
    },

    // onLoad () {},

    start() {
        this.spawnPlatform();
    },

    spawnPlatform() {

        //カラーオブジェクトの生成。
        var color = cc.instantiate(this.colorPrefab);
        color.parent = this.colorParent;

        //位置情報設定。
        var pos = this.prevNode.getPosition();
        var _pf = this.prevNode.getComponent('DestroyObject');
        if (_pf.getIsTube() == true)
            pos.y += this.prevNode.height * this.prevNode.scale / 2 + 150;
        else
            pos.y += this.prevNode.height * this.prevNode.scale / 2 + 200;

        color.setPosition(pos);

        //プレハブの高さに合わせて間隔を空ける。
        var rand = Math.floor(Math.random() * 22);
        var obj = cc.instantiate(this.platforms[rand]);
        var pf = obj.getComponent('DestroyObject');
        obj.parent = this.platformParent;
        pos = this.prevNode.getPosition();
        pos.y += this.prevNode.height * this.prevNode.scale / 2 + 300 + obj.height * obj.scale / 2;
        if (rand == 19 || rand == 20 || rand == 21)
            pos.y += 100;
        obj.setPosition(pos);


        //筒状かどうかでタグ分けする。
        if (rand == 0 || rand == 1 || rand == 2 || rand == 3 || rand == 4 || rand == 9 || rand == 10 || rand == 19 || rand == 20 || rand == 21)
            pf.setIsTube(true);

        this.prevNode = obj;

        var _star = this.starPrefab;
        //暗闇モードなら1/5の確率で3倍星を生成。〇▽の時は確定にする。
        var isClassic = Number(cc.sys.localStorage.getItem('MODE'));
        if (isClassic == 0) {
            var rand2 = Math.floor(Math.random() * 4) + 1;
            if (rand2 == 1 || rand == 19)
                _star = this.specialPrefab;
        }

        var star = cc.instantiate(_star);
        star.parent = this.starParent;

        //真ん中が空いてるオブジェクトかどうかで位置が変わる。
        if (pf.getIsTube() == true)
            star.setPosition(obj.getPosition());
        else {
            pos = this.prevNode.getPosition();
            pos.y += this.prevNode.height * this.prevNode.scale / 2 + 100;
            star.setPosition(pos);
        }

        //三角のオブジェクトならタグをつける。
        if (rand == 19 || rand == 20) {
            var collider = color.getComponent(cc.CircleCollider);
            collider.tag = 1;
        }
        else if (rand == 21) {
            var collider = color.getComponent(cc.CircleCollider);
            collider.tag = 2;
        }
    },

    // update (dt) {},
});
