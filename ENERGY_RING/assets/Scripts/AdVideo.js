
//IDを直入力しないとエラーが発生するので注意。

var preloadedRewardedVideo = null;

cc.Class({
    extends: cc.Component,

    properties: {

        isPossible: false, //表示可能か？初期化できたかどうかで値が変わる。
    },

    // onLoad () {},

    getIsPossible() {
        return this.isPossible;
    },

    start() {
        this.initialize();
    },

    //広告情報の設定
    initialize() {
        var self = this;

        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            return;
        }


        FBInstant.getRewardedVideoAsync(
            '301556503869023_301557183868955',
        ).then(function (rewarded) {
            self.preloadedRewardedVideo = rewarded;
            return self.preloadedRewardedVideo.loadAsync();
        }).then(function () {
            console.log('Video：Initialize success.');
        }).catch(function (err) {
            console.log('Video：Initialize failed.')
        });
    },

    //広告表示
    showAd() {
        var self = this;

        //PCかスマホかで処理を分ける。
        if (navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/i)) {

            if (self.preloadedRewardedVideo == null || typeof self.preloadedRewardedVideo == 'undefined')
                return;

            self.preloadedRewardedVideo.showAsync()
                .then(function () {
                    console.log('表示成功');
                    cc.sys.localStorage.setItem('Continue', 1);     //コンティニュー。
                })
                .catch(function (e) {
                    console.log(e.message);
                });
        }
        //PCでは動画が表示できないので代わりにインタースティシャルを表示させる。報酬付き。
        else {
            var adInter = cc.find('Canvas/Ads').getComponent('AdInterstitial');
            adInter.showAd(true);
            cc.sys.localStorage.setItem('Continue', 1);
        }

        var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
        canvasManager.remakeLayout();
    },

    // update (dt) {},
});
