

cc.Class({
    extends: cc.Component,

    properties: {
        mode: cc.Label,
        isClassic: true,

        board: cc.Node,

        adText: cc.Node,

        tutoBoard: cc.Node,
        tuto1: cc.Node,
        tuto2: cc.Node,

        interIsPossible: false,
    },

    // onLoad () {},
    start() {
    },

    setInterPossible() {
        this.interIsPossible = true;
    },

    preLoad() {
        if (cc.director.getScene().name == 'End') {

            this.judgeIsPossible();

            var continueNum = cc.sys.localStorage.getItem('Continue');
            if (continueNum != null && continueNum == 1) {
                console.log('コンティニュー状態解除')
                cc.sys.localStorage.setItem('Continue', 0);
                this.remakeLayout();
            }

            var filter = cc.find('Canvas/TouchFilter');
            filter.active = false;

            var rand = Math.floor(Math.random() * 10);
            if (rand < 3) {
                var adInter = cc.find('Canvas/Ads').getComponent('AdInterstitial');
                adInter.showAd();
            }
        }
    },

    changeMode() {
        this.isClassic = !this.isClassic;

        if (this.isClassic)
            this.mode.string = 'CLASSIC';
        else
            this.mode.string = 'DARKNESS';
    },

    startGame() {
        cc.sys.localStorage.setItem('MODE', this.isClassic ? 1 : 0);

        var sceneManager = cc.find('Canvas/SceneManager').getComponent('SceneManager');
        sceneManager.change();
    },

    showRanking() {
        this.board.active = true;
    },

    //広告を表示できるかどうかを判定。
    judgeIsPossible() {

        //両方とも広告が取得できていない場合は広告ボタンは表示しない。
        if (!this.interIsPossible) {
            this.remakeLayout();
            return;
        }
        // console.log('広告表示可能')
        this.adText.active = true;
    },

    //広告視聴後のゲームオーバー画面のレイアウトの作り直し。
    remakeLayout() {
        // console.log('両方とも表示できません')
        this.adText.active = false;
    },

    showTutorial() {
        this.tutoBoard.active = true;
    },

    changePage() {
        this.tuto1.active = !this.tuto1.active;
        this.tuto2.active = !this.tuto2.active;
    },

    // update (dt) {},
});
