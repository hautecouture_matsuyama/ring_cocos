cc.Class({
    extends: cc.Component,

    properties: {
        target:{
            default: null,
            type: cc.Node
        },
    },

    Close: function(){
        this.target.active = false;
    },
});
