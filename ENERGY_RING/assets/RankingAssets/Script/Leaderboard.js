var bordName = "ring_rank_s1";

cc.Class({
    extends: cc.Component,

    properties: {
        content:{
            default: null,
            type: cc.Node
        },

        rankPrefab:{
            default: null,
            type: cc.Prefab
        },

        myRankNode:{
            default: null,
            type: cc.Node
        },
    },

    //ランキングが表示されたらデータを取得する
    onEnable(){
        this.getMyRankData(this.myRankNode);
        this.getRankingData(this.rankPrefab);
    },

    //閉じたらデータを削除（そのうち変更したい）
    onDisable(){
        this.content.destroyAllChildren();
    },

    //スコア送信
    sendScore(score){
        if (typeof FBInstant === 'undefined')
            return;

        FBInstant
        .getLeaderboardAsync(bordName)
        .then(leaderboard => {
            console.log(leaderboard.getName());
            return leaderboard.setScoreAsync(score, '{race: "elf", lavel:3');
        })
        .then(()=> console.log('save'))
        .catch(error => console.error(error));

        FBInstant.updateAsync({
            action: 'LEADERBOARD',
            name: bordName
          })
            .then(() => console.log('Update Posted'))
            .catch(error => console.error(error));
    },

    //自分（Player）のランキング情報を取得
    getMyRankData(myRankNode){
        if (typeof FBInstant === 'undefined') 
        return;

        var playerIcon = myRankNode.getChildByName('player_icon').getComponent(cc.Sprite);
        var name = myRankNode.getChildByName('name_text').getComponent(cc.Label);
        var ranks = myRankNode.getChildByName('ranks');
        var rank = ranks.getChildByName('rank_text').getComponent(cc.Label);
        var score = myRankNode.getChildByName('score_text').getComponent(cc.Label);

        FBInstant
        .getLeaderboardAsync(bordName)
        .then(leaderboard => leaderboard.getPlayerEntryAsync())
        .then(entry => {
            var photoUrl = entry.getPlayer().getPhoto();
            cc.loader.load(photoUrl, (err, texture) => {
            playerIcon.spriteFrame = new cc.SpriteFrame(texture);
            });
            rank.string = this.setRankPlace(entry.getRank());
            name.string = entry.getPlayer().getName();
            score.string = entry.getScore();
        }).catch(error => {
                var photoUrl = FBInstant.player.getPhoto();
                cc.loader.load(photoUrl, (err, texture) => {
                    playerIcon.spriteFrame = new cc.SpriteFrame(texture);
                });
                rank.string = '';
                name.string = FBInstant.player.getName();
                score.string = '0';
        });
   },

    //１位から指定した順位までを取得
    getRankingData(rankPrefab){
        if (typeof FBInstant === 'undefined') 
            return;

        var self = this;

        FBInstant
        .getLeaderboardAsync(bordName)
        .then(leaderboard => leaderboard.getEntriesAsync(20, 0))
        .then(entries => {
        for (var i = 0; i < entries.length; i++) {

            var rankingData = entries[i];
            self.setRankingData(rankPrefab, rankingData);
        }
       
        }).catch(error => console.error(error));
   },


   //取得した順位を元にランキングを生成
   setRankingData(rankPrefab, rankingData){
       var self = this;
       var photoUrl;//= new Array();
       var icon;// = new Array();

       var rankNode = cc.instantiate(rankPrefab);
       self.content.addChild(rankNode);
       rankNode.setPosition(0,0);

       var playerIcon = rankNode.getChildByName('player_icon').getComponent(cc.Sprite);
       var name = rankNode.getChildByName('name_text').getComponent(cc.Label);
       var ranks = rankNode.getChildByName('ranks');
       var rank = ranks.getChildByName('rank_text').getComponent(cc.Label);
       var score = rankNode.getChildByName('score_text').getComponent(cc.Label);

       icon = playerIcon;
       photoUrl = rankingData.getPlayer().getPhoto();

       rank.string = this.setRankPlace(rankingData.getRank());
       name.string = rankingData.getPlayer().getName();            
       score.string = rankingData.getScore();

       cc.loader.load(photoUrl, (err, texture) => {
           var index = photoUrl.indexOf(texture.url);
           icon.spriteFrame = new cc.SpriteFrame(texture);
    });

   },


   //順位の後につける値
   setRankPlace(rank){
       var number = 'th';
       switch(rank){
           case 1:
                number = 'st';
                break;
           case 2:
                number = 'nd';
                break;
           case 3:
                number = 'rd';
                break;
       }
       return rank + number;
   },


score(){
    var self = this;
    self.sendScore(114);
}

});
